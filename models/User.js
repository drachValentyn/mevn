const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  firstName: String,
  lastName: String,
  age: Number,
  role: String,
  date: { type: Date, default: Date.now }
})

module.exports = mongoose.model('User', UserSchema)
